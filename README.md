### 1 What is ES6
    * 1995 - JS created
    * 1997 - ES 1
    * 2009 - ES 5
    * 2015 - ES 6
        - New keyword
        - Classes
        - Template string
        - Function: default parameter, arrow functions
### 2 Transpiling ES6
    * Babel.js
        - Use babel.js to convert ES6 => ES5 code   
        - Used frequently with React
    * Can automated transpiling using webpack
    * Currently most browser support all ES6 features
### 3 Syntax
    * let
    * const
    * template string: allow formatting JS code with variables
    * spread operator: ...
        - Can display all array into 1 array instead array in array

### 4 Function & Object
    * Default function parameters: 
        - A function that has its parameters have default values
        - When call that function with out passing any parameter, default values will be used
    * Enhancing object literals
        - Shorten object format    
    * Arrow function:  
        - Make JS function more readable & compact
        - Deal with scope of 'this'     
    * Destructuring assignment
        - Extract data from arrays & object, assign them to variables
    * Generators
        - Pause the function
        - Create asynchronous function
### 5 Classes     
    * ES 6 supporting create classes
    * Create class using JS:
        ```
            class ClassName {
                constructor(properties...){

                }
            }
        ```       
    * Inheritance:
        class A extends Class B {
            
        }    
    * Create class using React.js
        ```
            class A extends React.Component {
                render() {
                    return (<div>
                            <h1>{this.props.name}</h1>
                        </div>)
                }
            }

            React.render(<A name="Some_name"></Restaurant>, document.body);
        ```    
        
        - React classes also have constructor like JS classes