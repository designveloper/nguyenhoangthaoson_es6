var person = {
    first: "Doug",
    actions: ['bike', 'hike', 'ski', 'surf'],
    printActions() {
        this.actions.forEach(action => {
            var str = this.first + " likes to " + action;
            console.log(str);
        });
    }
};
person.printActions();