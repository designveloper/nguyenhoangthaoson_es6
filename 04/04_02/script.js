var cat = {
    meow(times) {
        console.log("meow".repeat(times));
    },
    purr(times) {
        console.log("purr".repeat(times));
    },
    snore(times) {
        console.log("ZZZ".repeat(times));
    }
};

cat.meow(1);
cat.purr(2);
cat.snore(3);